﻿using NotFoundChannel.Models.Dto;
using System.Collections.Generic;

namespace NotFoundChannel.ViewModels
{
    public class TopViewModel
    {
        public Message Message { get; set; }
        public Comment Comment { get; set; }
        public IEnumerable<Message> Messages { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        public IEnumerable<string> ErrorMessage { get; set; }
    }
}