﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NotFoundChannel.Models.Dto;
using NotFoundChannel.Models.Service;
using NotFoundChannel.ViewModels;

namespace NotFoundChannel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            TopViewModel view = new TopViewModel();
            view.ErrorMessage = (List<string>)TempData["EroorMessages"] ?? new List<string>();
            view.Message = (Message)TempData["Message"] ?? new Message();
            view.Comment = (Comment)TempData["Comment"] ?? new Comment();
            view.Messages = MessageService.SelectMessages();
            view.Comments = CommentService.SelectComments();
            return View();
        }

        public ActionResult Messages()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}