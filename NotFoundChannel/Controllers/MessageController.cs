﻿using NotFoundChannel.Models.Dto;
using NotFoundChannel.Commons;
using NotFoundChannel.Models.Service;
using System.Collections.Generic;
using System.Web.Mvc;

namespace NotFoundChannel.Controllers
{
    public class MessageController : Controller
    {
        [HttpPost]
        public ActionResult Insert(Message message)
        {
            List<string> eroorMessages = new List<string>();

            //入力が正しい場合
            if (IsValid(message.Text, eroorMessages))
            {
                MessageService.Insert(message);
            }
            else
            {
                TempData["EroorMessages"] = eroorMessages;
                TempData["Message"] = message;
            }

            return RedirectToAction("index", "Home");
        }

        private bool IsValid(string text, List<string> eroorMessages)
        {
            //つぶやきが未入力の場合
            if (string.IsNullOrEmpty(text))
            {
                eroorMessages.Add(ErrorMessage.MESSAGE_NOT_ENTERED);
            }
            //つぶやきが140文字以下ではない場合
            else if (140 < text.Length)
            {
                eroorMessages.Add(ErrorMessage.MESSAGE_LIMIT);
            }

            if (eroorMessages.Count != 0)
            {
                return false;
            }
            return true;
        }
    }
}
