﻿using MySql.Data.MySqlClient;
using NotFoundChannel.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace NotFoundChannel.Models.Dao
{
    public class CommentDao
    {
        public static void InsertComment(MySqlConnection connection, Comment comment)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO comments ( ");
            sql.Append("    user_id, ");
            sql.Append("    text, ");
            sql.Append("    created_date, ");
            sql.Append("    updated_date ");
            sql.Append(") VALUES ( ");
            sql.Append("    @user_id, ");
            sql.Append("    @text, ");
            sql.Append("    CURRENT_TIMESTAMP, ");
            sql.Append("    CURRENT_TIMESTAMP ");
            sql.Append(")");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);
            command.Parameters.Add(new MySqlParameter("user_id", comment.MessagesId));
            command.Parameters.Add(new MySqlParameter("text", comment.Text));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        public static List<Comment> SelectComments(MySqlConnection connection)
        {
            MySqlDataReader reader = null;

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT * from comments");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);

            try
            {
                reader = command.ExecuteReader();
                List<Comment> comments = ToComments(reader);

                return comments;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
            }
        }
        private static List<Comment> ToComments(MySqlDataReader reader)
        {
            try
            {
                List<Comment> comments = new List<Comment>();
                while (reader.Read())
                {
                    Comment comment = new Comment();
                    comment.Id = (int)reader["id"];
                    comment.MessagesId = (int)reader["messages_id"];
                    comment.Text = (string)reader["text"];
                    comment.CreatedDate = (DateTime)reader["created_date"];
                    comment.UpdatedDate = (DateTime)reader["Updated_date"];

                    comments.Add(comment);
                }
                return comments;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
