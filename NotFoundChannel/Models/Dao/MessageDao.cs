﻿using MySql.Data.MySqlClient;
using NotFoundChannel.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace NotFoundChannel.Models.Dao
{
    public class MessageDao
    {
        public static void InsertMessage(MySqlConnection connection, Message message)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO messages ( ");
            sql.Append("    text, ");
            sql.Append("    created_date, ");
            sql.Append("    updated_date ");
            sql.Append(") VALUES ( ");
            sql.Append("    @text, ");
            sql.Append("    CURRENT_TIMESTAMP, ");
            sql.Append("    CURRENT_TIMESTAMP ");
            sql.Append(")");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);

            command.Parameters.Add(new MySqlParameter("text", message.Text));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        public static List<Message> SelectMessages(MySqlConnection connection)
        {
            MySqlDataReader reader = null;

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT * from messages");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);

            try
            {
                reader = command.ExecuteReader();
                List<Message> messages = ToMessages(reader);

                return messages;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
            }
        }
        private static List<Message> ToMessages(MySqlDataReader reader)
        {
            try
            {
                List<Message> messages = new List<Message>();
                while (reader.Read())
                {
                    Message message = new Message();
                    message.Id = (int)reader["id"];
                    message.Text = (string)reader["text"];
                    message.CreatedDate = (DateTime)reader["created_date"];
                    message.UpdatedDate = (DateTime)reader["Updated_date"];

                    messages.Add(message);
                }
                return messages;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
