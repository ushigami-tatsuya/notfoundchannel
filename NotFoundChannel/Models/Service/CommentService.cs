﻿using MySql.Data.MySqlClient;
using NotFoundChannel.Models.Dao;
using NotFoundChannel.Models.Dto;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace NotFoundChannel.Models.Service
{
    public class CommentService
    {
        private static readonly string connectionStrings = ConfigurationManager.ConnectionStrings["mysql"].ConnectionString;

        public static void Insert(Comment comment)
        {
            MySqlConnection connection = null;
            MySqlTransaction transaction = null;
            try
            {
                connection = new MySql.Data.MySqlClient.MySqlConnection(connectionStrings);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);

                CommentDao.InsertComment(connection, comment);
                transaction.Commit();
            }
            catch (SqlException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public static List<Comment> SelectComments()
        {
            MySqlConnection connection = null;
            MySqlTransaction transaction = null;
            try
            {
                connection = new MySql.Data.MySqlClient.MySqlConnection(connectionStrings);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);

                List<Comment > comments = CommentDao.SelectComments(connection);
                transaction.Commit();

                return comments;
            }
            catch (SqlException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
