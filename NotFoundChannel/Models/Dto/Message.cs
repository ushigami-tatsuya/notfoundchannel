﻿using System;
using System.ComponentModel;

namespace NotFoundChannel.Models.Dto
{
    public class Message
    {
        [DisplayName("ID")]
        public int Id { get; set; }

        [DisplayName("スレッドタイトル")]
        public string Text { get; set; }

        [DisplayName("稼働状態")]
        public int IsStopped { get; set; }

        [DisplayName("作成日時")]
        public DateTime CreatedDate { get; set; }

        [DisplayName("更新日時")]
        public DateTime UpdatedDate { get; set; }
    }
}